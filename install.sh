#!/bin/bash

dir=~/dotfiles
olddir=~/dotfiles_old

# list dotfiles without the dots
files="zshrc vimrc bashrc tmux.conf gemrc"

mkdir -p $olddir

cd $dir

for file in $files; do
  touch $file
  #create the symlink
  ln -sf $dir/$file ~/.$file
done

# Use vimrc as neovims config file
ln -sf ~/vim ~/.config/nvim
ln -sf ~/dotfiles/init.vim ~/.vim/init.vim

# Create backup dir
mkdir -p ~/.vim/backup

# Copy vim help navigation helpers
mkdir -p ~/.vim/ftplugin
ln -sf ~/dotfiles/help.vim ~/.vim/ftplugin/help.vim

# Directories
ln -sf ~/dotfiles/UltiSnips ~/.vim/UltiSnips
ln -sf ~/dotfiles/UltiSnips ~/.nvim/UltiSnips
ln -sf ~/dotfiles/atom ~/.atom

mkdir -p ~/.vim/bundle
# git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
mkdir -p ~/.vim/autoload
curl -fLo ~/.vim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Also install tmux clipboard for copy/paste in tmux
# brew install reattach-to-user-namespace --wrap-pbcopy-and-pbpaste
