"
"        _/_/_/       _/_/_/         _/_/        _/_/_/
"     _/             _/    _/     _/    _/      _/    _/
"    _/  _/_/       _/_/_/       _/    _/      _/    _/
"   _/    _/       _/    _/     _/    _/      _/    _/
"    _/_/_/       _/    _/       _/_/        _/_/_/
"
" Gustav Buchholtz's vimrc v3.0

" INITIALIZE {{{

" No backwards compability
set nocompatible

" }}}

" BASICS {{{

" Command history
set history=1000

" Autoread when file is changed from outside of vim
set autoread

" Don't update screen during macros
set lazyredraw

" Because we're not on a thin terminal these days
set ttyfast

" Allow hidden buffers 
set hidden

" Enable mouse
set mouse+=a

" To make window resizing work with tmux
" set ttymouse=xterm2

" Don't wrap lines
set nowrap

" Centralized backup dir
set backupdir=~/.vim/backup

" No swap
set noswapfile

" Allow visual block to go past end of line
set virtualedit=block

" Allow backspace to remove more stuff in insert mode
set backspace=indent,eol,start

" Eliminate delay after pressing escape
set timeoutlen=1000 ttimeoutlen=0

" Use UTF-8
set encoding=utf-8
setglobal fileencoding=utf-8

" Swedish and English spelling
set spelllang=en,sv

" Show line numbers in netrw
let g:netrw_bufsettings = 'number'

" }}}

" KEY BINDINGS {{{

" Leader key
let mapleader = "\<Space>"
let g:mapleader = "\<Space>"

" remap to run macro in register 'q' instead of annoying Ex mode
nnoremap Q @q
vnoremap Q @q

" no more hitting ZZ (write and quit) by accident when trying to indent
nnoremap ZZ <nop>

" $ is pretty awkward to press, so we remap it to go to last non-blank char on
" current row.
" Nice because _ is first non-blank char on line
nnoremap - g_
vnoremap - g_

" Quicker ESC from insert mode
inoremap jj <esc>

" Navigate by display row, not by line - for wrapped lines
noremap j gj
noremap k gk

" I use block visual mode more often than normal visual, so lets switch them.
nnoremap v <C-v>
nnoremap <C-v> v
vnoremap v <C-v>
vnoremap <C-v> v

" Make Y behave like C and D - operate on rest of line
nnoremap Y y$

" Shorter commands for navigating between windows
noremap <c-h> <c-w>h
noremap <c-j> <c-w>j
noremap <c-k> <c-w>k
noremap <c-l> <c-w>l

" Set pastemode and paste from clipboard 
nnoremap <Leader>p :set paste<CR>o<esc>"*p:set nopaste<cr>
nnoremap <Leader>P :set paste<CR>O<esc>"*p:set nopaste<cr>
vnoremap <leader>y "+ygv

" Open split windows and jump to them
nnoremap <leader>w <c-w>v<c-w>l
nnoremap <leader>W <c-w>s<c-w>j

" Quick open vimrc
nnoremap <leader>vm :sp<cr>:e ~/dotfiles/vimrc<cr> 

" Bash type jumps for commandline
cnoremap <c-a> <home>
cnoremap <c-e> <end>

" Map swedish keys to something useful.
nnoremap ö [
nnoremap ä ]
nnoremap Ö {
nnoremap Ä }
nnoremap <c-ö> <c-[>
nnoremap <c-ä> <c-]>

" Move between quick fix list.
nnoremap äq :cnext<CR>
nnoremap öq :cprev<CR>
" Move between buffers.
nnoremap äb :bnext<CR>
nnoremap öb :bprev<CR>


"Switch forward tick and back tick in normal mode for easier mark-jumps
nnoremap ` ´
nnoremap ´ `
vnoremap ` ´
vnoremap ´ `

" Navigate buffers with arrow keys
nnoremap <Left> :bprev<CR>
nnoremap <Right> :bnext<CR>

" Clear highlighted search
nnoremap <silent><leader>n :noh<CR>

" }}}

" Neovim, MacVim and gVim {{{

if has('gui_macvim')
  set guioptions-=T " hides toolbar
  set guioptions-=L " hides left scroll
  set guioptions-=R " hides right scroll
  set guioptions-=r " hides right scroll
  set guioptions-=B " hides bottom scroll

  set transparency=5 " I do like transparent windows

  set guifont=Menlo:h14

  " Because MacVim doesn't play very nice with zsh 
  set shell=/bin/bash
endif

if has('gui_gtk') || has('gui_gtk2')
  set guioptions-=T " hides toolbar
  set guioptions-=L " hides left scroll
  set guioptions-=R " hides right scroll
  set guioptions-=r " hides right scroll
  set guioptions-=B " hides bottom scroll
  set guifont=UbuntuMono\ 13
endif

if has('nvim')
  " neovim bug. Mapping <c-h> to switch pane left isn't working.
  " TODO: remove this when bug is fixed
  nmap <BS> <C-W>h
  " get to normal model in terminal mode
  " tnoremap <ESC> <c-\><c-n>
  tnoremap jj <c-\><c-n>

  let g:python_interpreter='python2'
  " Load Python
  runtime! plugin/python_setup.vim
endif

" }}}

" PLUGS {{{

" Init vim-plug. This must be initialized before loading plugins
call plug#begin('~/.vim/plugged')

" NeoSnippet {{{

Plug 'Shougo/neosnippet'
let g:neosnippet#enable_snipmate_compatibility = 1
let g:neosnippet#disable_runtime_snippets = { '_' : 1 } "Disable all built in snippets
let g:neosnippet#snippets_directory='~/.vim/snippets/'
imap <c-l> <plug>(neosnippet_expand_or_jump)
vmap <c-l> <plug>(neosnippet_expand_target)

" }}}
" Ultisnips {{{

" Code snippets engine.
" Plug 'sirver/ultisnips'
"
" let g:UltiSnipsExpandTrigger="<c-l>"
" let g:UltiSnipsJumpForwardTrigger="<c-l>"

" let g:UltiSnipsSnippetsDir="~/.vim/UltiSnips"
" let g:UltiSnipsSnippetDirectories=["UltiSnips"]

"}}}
" YouCompleteMe / DeoPlete {{{

if has('nvim')
  " Deoplete for nvim
  let g:deoplete#enable_at_startup = 1

  function! DoRemote(arg)
    UpdateRemotePlugins
  endfunction
  Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }

  " Close scratch window on completion
  autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

  imap <expr><C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
  imap <expr><C-k> pumvisible() ? "\<C-p>" : "\<C-k>"

  " <Tab> completion:
  " 1. If popup menu is visible, select and insert next item
  " 2. Otherwise, if within a snippet, jump to next input
  " 3. Otherwise, if preceding chars are whitespace, insert tab char
  " 4. Otherwise, start manual autocomplete
  imap <silent><expr><Tab> pumvisible() ? "\<C-n>"
    \ : (neosnippet#jumpable() ? "\<Plug>(neosnippet_jump)"
    \ : (<SID>is_whitespace() ? "\<Tab>"
    \ : deoplete#mappings#manual_complete()))

  smap <silent><expr><Tab> pumvisible() ? "\<C-n>"
    \ : (neosnippet#jumpable() ? "\<Plug>(neosnippet_jump)"
    \ : (<SID>is_whitespace() ? "\<Tab>"
    \ : deoplete#mappings#manual_complete()))

  inoremap <expr><S-Tab>  pumvisible() ? "\<C-p>" : "\<C-h>"

  function! s:is_whitespace()
    let col = col('.') - 1
    return ! col || getline('.')[col - 1] =~? '\s'
  endfunction

  " Tern for deoplete. Make sure tern is installed: npm i -g tern
  Plug 'carlitux/deoplete-ternjs'

else
  " Use YouCompleteMe for regular vim
  "Plug 'Valloric/YouCompleteMe', { 'do': './install.sh' }
endif

" }}}
" Tern {{{

Plug 'ternjs/tern_for_vim', {'do': 'npm install'}

nnoremap <leader>d :TernDef<cr>
nnoremap <leader>D :TernDefSplit<cr>

" }}}
" NERDTree {{{

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
nnoremap <leader>. :NERDTreeToggle<cr>
"}}}
" FZF {{{

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
let $FZF_DEFAULT_COMMAND = 'ag -l --nocolor | egrep -v ".*(ico|png|jpe?g|gif|svg|ttf|otf|eot|woff|map|dat)\$"'
nnoremap <c-p> :FZF<cr>

" }}}
" Vim-tmux-navigator {{{

Plug 'christoomey/vim-tmux-navigator'

" }}}
" Ag {{{

" Searching with silver searcher
Plug 'rking/ag.vim', {'on': 'Ag'}

" }}}
" Syntastic / NeoMake {{{

if has('nvim') " NeoMake for nvim
  Plug 'neomake/neomake'
  " Run NeoMake on read and write operations
  " let g:neomake_javascript_enabled_makers = ['eslint']
  " let g:neomake_php_enabled_makers = ['php']
  let g:neomake_open_list = 2


  let g:neomake_warning_sign = {
    \ 'text': '😡 ',
    \ 'texthl': 'WarningMsg',
    \ }
  let g:neomake_error_sign = {
    \ 'text': '😨 ',
    \ 'texthl': 'ErrorMsg',
    \ }

  autocmd! BufReadPost,BufWritePost * Neomake

else  " Syntastic for regular vim
  " Run your code through a linter or parser and present the errors and warnings in the gutter
  Plug 'scrooloose/syntastic'

  " The symbol in the gutter
  highlight SyntasticWarningSign guifg=black guibg=red ctermfg=black ctermbg=red
  highlight SyntasticErrorSign guifg=white guibg=red ctermfg=white ctermbg=red
  " Use these Icons/chars
  " let g:syntastic_error_symbol = '✗ '
  " let g:syntastic_warning_symbol = '!'
  let g:syntastic_error_symbol = '😡 '
  let g:syntastic_warning_symbol = '😨'
  " Highlight whole line
  "highlight SyntasticWarningLine guifg=black guibg=#aa5555
  "highlight SyntasticErrorLine guifg=white guibg=red
  " Column pointer
  highlight SyntasticError ctermbg=red ctermfg=white guibg=red guifg=white
  highlight SyntasticWarning ctermbg=red ctermfg=white guibg=red guifg=white
endif

" }}}
" Fugitive {{{

" Plug for working with GIT
Plug 'tpope/vim-fugitive'

nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <F2> :Gstatus<CR>                  " Use F2 to show git status
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
" nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>

function! PushToCurrentBranch()
  exe ":Gwrite"
  let branch = fugitive#statusline()
  let branch = substitute(branch, '\c\v\[?GIT\(([a-z0-9\-_\./:]+)\)\]?', $BRANCH.' \1', 'g')
  exe ":Git push origin" . branch
endfunction

function! AddCurrentFile()
  exe ":Git add " . @%
endfunction

nnoremap <leader>gw :call PushToCurrentBranch()<cr>
nnoremap <leader>ga :call AddCurrentFile()<cr>
"}}} 
" Gitv {{{

" Nice git log visualization
Plug 'gregsexton/gitv', {'on': 'Gitv'}
nnoremap <silent> <leader>gl :Gitv<CR>

" Do not map control key for shortcuts
let g:Gitv_DoNotMapCtrlKey = 1
" Clear buffers
let g:Gitv_WipeAllOnClose = 0

" }}}
" Surround {{{

" Surround textblocks with whatever bracket, quote, paranthesis you need
Plug 'tpope/vim-surround'

"}}} 
" tcomment-vim {{{

Plug 'tomtom/tcomment_vim'

" }}}
" Vim repeat {{{

" Extend repeat(.) to work with plugins as well
Plug 'tpope/vim-repeat'

"}}} 
" CSS colors {{{

" Show css-color in vim by highlighting the value.
Plug 'gorodinskiy/vim-coloresque', {'for': ['css', 'scss', 'sass']}

"}}} 
" Vim-sneak {{{

" Like f and t, but you provide it with two characters for better jump precision.
Plug 'justinmk/vim-sneak'
let g:sneak#s_next = 1

"}}} 
" Easymotion {{{

" Fast jump to position
Plug 'Lokaltog/vim-easymotion'

" }}}
" targets.vim {{{
 
" Add more text objects
"( ) b (work on parentheses)
"{ } B (work on curly braces)
"[ ] r (work on square brackets)
"< > a (work on angle brackets)
"t (work on tags)
" also works on periods and commas

Plug 'wellle/targets.vim'



" }}}
" Color schemes {{{

" My collection of colorschemes
Plug 'grod/grod-vim-colors'

" Hybrid
Plug 'w0ng/vim-hybrid'

" Oceanic next
Plug 'mhartington/oceanic-next'

" Kalisi
Plug 'freeo/vim-kalisi'

" }}}
" Editorconfig {{{

" Read .editorconfig file, and uses it's settings
Plug 'editorconfig/editorconfig-vim'

" }}}
" Airline {{{

Plug 'bling/vim-airline'
let g:airline_powerline_fonts = 1
" let g:airline_theme='oceanicnext'
let g:airline_right_sep=''
let g:airline_left_sep=''

" }}}
" Tabular {{{

" Align codeblocks by a common delimiter
Plug 'godlygeek/tabular'

"}}}
" delimtMate {{{

" For auto-inserting matching pairs: ()[]{}<>''""
Plug 'Raimondi/delimitMate'

" }}}
" Clever-F {{{

Plug 'rhysd/clever-f.vim'
" Extends f, F, t and T. Instead of using ; and , to jump back and forth. Just
" press f, F, t or T again

"}}}
" LiveDown {{{

" Live markdown preview in chrome
Plug 'shime/vim-livedown'

" }}}
" PHP Indenting {{{

Plug '2072/PHP-Indenting-for-VIm'

" }}}
" YAJS Javascript Syntax {{{

Plug 'othree/yajs.vim'

" }}}

" Post Plugin {{{

" End vim-plug
call plug#end()
filetype plugin on
filetype indent on

"}}}

"}}} " End of bundles

" LOOK AND FEEL {{{

" Syntax highlighting
syntax enable

" 256 colors
set t_Co=256
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

colorscheme Tomorrow-Night-Eighties

set background=dark

" Different color for cursor in terminal mode
highlight TermCursor ctermfg=green guifg=green

" Highlight current row
" set cursorline

" Command bar height
set cmdheight=1

" Show matching braces
set showmatch

" Visualbell instead of beeping
set visualbell

" Solid line for vertical split
set fillchars=vert:\|

" Hidden characters
set listchars=trail:•,tab:▸\ ,extends:❯,precedes:❮,nbsp:␣,eol:↪

"set scrolljump=6 " Scroll this many lines when reaching 'scrolloff'
set scrolloff=6 " Scroll when 6 lines from the screen edge

set showcmd " Show partial/unfinished key commands

set number " Show line numbers
set relativenumber " Relative line number

" Highlight the 80th column
set colorcolumn=80

set laststatus=2 " All windows have status lines

" Statusline
set statusline=
set statusline+=#%-5n    " Buffer number
set statusline+=%F       " Full path to file

set statusline+=%=       " Right align

set statusline+=%l       " Current line
set statusline+=,%v      " Virtual column number
set statusline+=\ -\ %P  " Percent of file
set statusline+=(%L)     " Total lines

" }}}

" CODING {{{

set expandtab " Use spaces instead of tabs
set smarttab " Backspace deletes a 'shiftwidth' worth of spaces
set shiftwidth=2 " This many spaces
set tabstop=2 " A Tab-char is also 2 chars
set shiftround " Round indent to nearest multiple of shiftwidth

set autoindent
set smartindent

" Default foldmethod
set foldmethod=marker

" }}}

" SEARCH & REPLACE {{{

" Go to first match if reaching end of file
set incsearch
" Highlight search matches
set hlsearch

set ignorecase
set smartcase
set wildmenu " tab completion in command line
set wildmode=list:longest,full " Make tab complition similar to bash

" ignore these folders
set wildignore+=*/dist/*,*/node_modules/*
set wildignore+=*/.sass-cache/*
set wildignore+=*/build/*,bin/*
set wildignore+=*/tmp/*
set wildignore+=*.so,*.swp,*.zip,.DS_Store
set wildignore+=*.hg,*.git
set wildignore+=*.ttf,*.otf,*.woff,*.eot,*.svg
set wildignore+=*.jpg,*.jpeg,*.png,*.gif,*.tiff,*.psd
set wildignore+=*.fla,*.swf

" Allow for searching all files and subdirectories of current directory
set path+=**

" Map ctrl-s to search and replace
nnoremap <c-s> :%s//g<LEFT><LEFT>

" I almost always want global and very magic
nnoremap / /\v/g<left><left>


" }}}

" HELPER FUNCTIONS {{{


" Write file as root
function! SudoWrite()
" cnoremap w!! w !sudo tee % >/dev/null
  exec '%write !sudo tee >/dev/null'
endfunc
" Create command so we don't have to use 'call' in command line
command! SudoWrite call SudoWrite()

" Populate args list with contents of quickfix list.
" Borrowed from Drew Neil https://github.com/nelstrom/vim-qargs
command! -nargs=0 -bar Qargs execute 'args' QuickfixFilenames()
function! QuickfixFilenames()
  " Building a hash ensures we get each buffer only once
  let buffer_numbers = {}
  for quickfix_item in getqflist()
    let buffer_numbers[quickfix_item['bufnr']] = bufname(quickfix_item['bufnr'])
  endfor
  return join(map(values(buffer_numbers), 'fnameescape(v:val)'))
endfunction

" Convert current markdown file to RTF with pandoc
function! Mdown2rtf()
  silent exec 'cd %:p:h'
  silent exec 'pwd'
  silent exec '!pandoc -s -f markdown -o '.expand("%:t:r").'.rtf %'
  :echo "created RTF from file"
endfunc
command! Mdown2rtf call Mdown2rtf()

" And to HTML
function! Mdown2html()
  silent exec 'cd %:p:h'
  silent exec 'pwd'
  silent exec '!pandoc -s -f markdown -o '.expand("%:t:r").'.html %'
  :echo "created HTML from file"
endfunc
command! Mdown2html call Mdown2html()

" }}}

" OTHER STUFF {{{

augroup filetypes
  " Treat .hbs (handlebars) as html
  au Bufread,BufNewFile *.hbs set filetype=html
  " Treat .as as actionscript instead of Atlas
  au Bufread,BufNewFile *.as set filetype=actionscript
augroup END

augroup misc
  " Resize splits when the window is resized
  au VimResized * exe "normal! \<c-w>="
augroup END

" Tell vim to remember certain things when we exit {{{
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
" set viminfo='10,\"100,:20,n~/.vim/.viminfo
if has('nvim')
  set viminfo='10,\"100,:20
  set viminfo+=n~/.nvim/tmpfiles/viminfo
endif

function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END
"}}}

" lastly, load local vimrc if it exists
if filereadable(expand("vimrc.local"))
  source vimrc.local
endif

" }}}
