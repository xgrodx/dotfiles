" Easy navigation in help files

" Jump to topic under cursor
nnoremap <buffer> <CR> <C-]>

" Return from last jump
nnoremap <buffer> <BS> <C-T>

" Find next option
nnoremap <buffer> o /'\l\{2,\}'<CR>

" Find previous option
nnoremap <buffer> O ?'\l\{2,\}'<CR>

" Find next subject
nnoremap <buffer> s /\|\zs\S\+\ze\|<CR>

" Find previous subject
nnoremap <buffer> S ?\|\zs\S\+\ze\|<CR>
