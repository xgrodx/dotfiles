# Gustav Buchholtz's zsh config file.

# Enable autocomplete
autoload -U compinit
compinit
# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# bindkey '^I' complete-word # complete on tab, leave expansion to _expand
#unsetopt menu_complete   # do not autoselect the first completion entry
#unsetopt flowcontrol
# setopt auto_menu         # show completion menu on succesive tab press
# setopt complete_in_word
#setopt always_to_end


# Load config files
for config_file (~/dotfiles/zsh/config/*.zsh); do
  source $config_file
done

# Load functions
for function_file (~/dotfiles/zsh/functions/*.zsh); do
  source $function_file
done

# Load Aliases
source ~/dotfiles/zsh/aliases.zsh

### Plugins

# For showing git status in terminal prompt 
# source ~/dotfiles/zsh/plugins/git-prompt/zshrc.sh

# Load theme
source ~/dotfiles/zsh/themes/avit.zsh
if [ $(hostname) = "spock" ]; then
  source ~/dotfiles/zsh/themes/dieter.zsh
fi

# Append various directories to PATH
export PATH=/opt/local/bin
export PATH=$PATH:/opt/local/sbin
export PATH=$PATH:/usr/local/mysql/bin
export PATH=$PATH:/usr/local/bin
export PATH=$PATH:/usr/local/git/bin
export PATH=$PATH:/usr/bin
export PATH=$PATH:/bin
export PATH=$PATH:/usr/sbin
export PATH=$PATH:/sbin
export PATH=$PATH:$HOME/dotfiles/scripts
export PATH=$PATH:$HOME/.composer/vendor/bin

export ANDROID_HOME=/Users/grod/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# PHP
export PATH="$PATH:$(brew --prefix homebrew/php/php55)/bin"

# For gems without rbenv
# export PATH=$PATH:$(ruby -rubygems -e "puts Gem.user_dir")/bin

# Ruby environment
# export PATH=$PATH:$HOME/.rbenv/shims
# export PATH=$HOME/.rbenv/bin:$PATH
# eval "$(rbenv init -)"

# Use mvim to start macvim from console
export PATH=$PATH:/usr/local/Cellar/macvim/HEAD/bin

# export PATH="/usr/local/heroku/bin:$PATH"
export LANG=en_US.UTF-8

# Use vim for git
export GIT_EDITOR=vim
export VISUAL=vim
export EDITOR=vim

# Aliases
alias tnew="tmux new-session -As `basename $PWD`"

# Use macvims terminal vim as default
# alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"

# 256 colors in neovim
#Neovim true color support
export NVIM_TUI_ENABLE_TRUE_COLOR=1
#Neovim cursor shape support
export NVIM_TUI_ENABLE_CURSOR_SHAPE=1
# alias v='NVIM_TUI_ENABLE_TRUE_COLOR=1 nvim'
# alias vim='NVIM_TUI_ENABLE_TRUE_COLOR=1 nvim'
alias la=ls -la

# Use homebrew ctags
alias ctags=/usr/local/Cellar/ctags/5.8_1/bin/ctags

# [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#

# Open visual studio code from terminal
function code {  
    if [[ $# = 0 ]]
    then
        open -a "Visual Studio Code"
    else
        local argPath="$1"
        [[ $1 = /* ]] && argPath="$1" || argPath="$PWD/${1#./}"
        open -a "Visual Studio Code" "$argPath"
    fi
}
