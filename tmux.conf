# 256 colors.
set -g default-terminal "screen-256color"

# increase scroll-back history
set -g history-limit 5000

# use vi key bindings
setw -g mode-keys vi

# copy and paste. Requires Tmux pasteboard
# brew install reattach-to-user-namespace --wrap-pbcopy-and-pbpaste
# or
# sudo port install tmux-pasteboard
set-option -g default-command "reattach-to-user-namespace -l zsh"

# start window index at 1
set -g base-index 1

# ..and pane index at 1
set -g pane-base-index 1

# Highlight window when it has new activity
setw -g monitor-activity on
set -g visual-activity on

# Re-number windows when one is closed
set -g renumber-windows on

# enable mouse, and the ability to resize panes by dragging
# set -g mode-mouse on
# setw -g mouse-select-window on
# setw -g mouse-select-pane on
# set -g mouse-resize-pane on

# Since tmux 2.1 we only have one mouse option:
setw -g mouse on

# No delay between commands
set -s escape-time 0

# Current spotify song
set -g status-right '#(~/dotfiles/scripts/tmux-spotify-info)'

# center status bar
set -g status-justify left

# Theme
source-file ~/dotfiles/tmux/themes/neodark

# Smart pane switching with awareness of vim splits
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)g?(view|n?vim?)(diff)?$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)g?(view|n?vim?)(diff)?$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)g?(view|n?vim?)(diff)?$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)g?(view|n?vim?)(diff)?$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n C-\ run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)g?(view|n?vim?)(diff)?$' && tmux send-keys 'C-\\') || tmux select-pane -l"
