# Use macvims terminal vim as default
# alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"

# Use neovim
# alias v='nvim'
# alias vim='nvim'
# alias vim='~/dotfiles/nvim-client'

# Aliases
alias la="ls -la"
alias tmux="TERM=screen-256color-bce tmux"
alias tnew="tmux new-session -As `basename $PWD`"
alias gits="git status"

alias gs="git status"

# If we're in neovim, we want to use neovim-remote
# https://github.com/mhinz/neovim-remote
if [ -n "${NVIM_LISTEN_ADDRESS+x}" ]; then
  alias h='nvr -o'
  alias v='nvr -O'
  alias t='nvr --remote-tab'
fi
