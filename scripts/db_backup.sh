#!/bin/bash
_now=$(date +"%m_%d_%Y_%T")
_file="/Users/grod/Desktop/db_dump_$_now.sql"
echo "Starting backup to $_file..."

mysqldump --opt --single-transaction -u root -p'root' database > "$_file"

echo "Backup complete"
